# Prueba técnica frontend 

**Crear una SPA con la siguientes caracteristicas:**

**Requerimiento:**
- Crear una landing page tipo tienda con vista de productos y carrito
- Utilizar un framework js (El de tu elección)
- Utilizar un framework css (El de tu elección)
- Programar logica para cada evento (Agregar al carrito, listar productos, eliminar del carrito, aumentar o disminuir cantidad)

**Vistas**

- Listado de productos (Obtener lista de productos del archivo products.json)
    - Vista individual del producto:
        - Contendrá un boton de agregar al carrito
        - Contendrá imagen de producto, precio y titulo
        [Referencia](https://i.gyazo.com/45840ddaf8cfe389235435cbbdfe4938.png)
    


- Vista de carrito
    - Listar productos agregados al carrito
        -   Vista individual del producto
            - Contendrá imagen de producto, precio, titulo  cantidad
                [Referencia](https://i.gyazo.com/493d6f2ccb9b1f4a2626dfc43307a3dc.png)

            - Contendrá un input para aumentar o disminuir la cantidad de cada producto
                [Referencia](https://gyazo.com/135cf1ef06f822547f82c85618b40966)
            
            - Contendrá un boton para eliminar un producto
    - Subtotales:
        - Calcular subtotal
        - Calcular impuestos
        - Calcular Monto total


    
    



 


